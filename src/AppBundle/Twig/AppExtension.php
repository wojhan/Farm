<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension{
    
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('betType', array($this, 'betTypeFilter')),
            new \Twig_SimpleFilter('betResult', array($this, 'betResultFilter')),
        );
    }
    
    public function betTypeFilter($bet){
        
        $name = [
            '',
            'RED',
            'GREEN',
            'BLACK',
            '1 TO 12',
            '13 TO 24',
            '25 TO 36'
        ];
        
        return $name[$bet];
        
    }
    
    public function betResultFilter($amount, $betType, $status){
        
        if($status){
            if($betType <= 3){
               $result = $amount*2;
            }
            if($betType >= 4){
               $result = $amount*3;
            }
            return $result;
        } else{
            return -($amount);
        }
        
    }
    
}