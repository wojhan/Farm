<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ItemNotFoundException;
use AppBundle\Exception\NotEnoughCoinsException;
use AppBundle\Exception\WrongCategoryException;
use AppBundle\Util\CoinManager;
use AppBundle\Util\ItemManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game/seed-shop")
 * @Security("has_role('ROLE_USER')")
 */
class SeedShopController extends Controller{
            
    /**
     * @Route("/", name="game_seed_shop")
     */
    public function indexAction(ItemManager $im){
        
        $user = $this->getUser();
        $items = $im->getSeeds($user);
        
        return $this->render('seedShop/seedShop.html.twig', array(
            'page_title' => 'SeedShop',
            'items' => $items
        ));
        
    }
    
     /**
     * @Route("/buy", name="game_buy_seed")
     * @Method("POST")
     */
    public function buySeedAction(CoinManager $cm, ItemManager $im, Request $request){
        $itemName = $request->request->get('name');
        $amount = $request->request->get('amount');
        $user = $this->getUser();
                
        try{
            $im->proceedShoppingTransaction($cm, $user, $itemName, $amount);
        } catch(ItemNotFoundException $ex){
            $this->addFlash('danger', $itemName." doesn't exists.");
            return new Response('no item');
        } catch(WrongCategoryException $ex){
            $this->addFlash('danger', 'Wrong way!');
            return new Response('wrong way');
        } catch(NotEnoughCoinsException $ex){
            $this->addFlash('danger', "You don't have enough money.");
            return new Response('no money');
        }
        
        $this->addFlash('success', 'You have just bought '.$amount.' '.$itemName);
        
        return new Response('done');
        
    }

}