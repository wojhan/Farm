<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Roulette as RoulleteEntity;
use AppBundle\Exception\ForbiddenActionException;
use AppBundle\Exception\NotEnoughCoinsException;
use AppBundle\Util\CoinManager;
use AppBundle\Util\Roulette;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game/roulette")
 * @Security("has_role('ROLE_USER')")
 */
class RouletteController extends Controller{
    
    /**
     * @Route("/", name="game_roulette")
     */
    public function rouletteAction(){
                
        return $this->render('roulette/roulette.html.twig', array(
           'page_title' => 'Roulette',
        ));
    }
    
    /**
     * @Route("/bet/{type}/{amount}", name="roulette_bet")
     */
    public function betAction($type, $amount, Roulette $roulette, CoinManager $cm){
        
        $user = $this->getUser();
        
        $response = array();
        $response['status'] = 'error';
        
        try{
            $number = $roulette->bet($user, $type, $amount, $cm);
            $response['status'] = 'ok';
            $response['message'] = $number;
        } catch (ForbiddenActionException $ex) {
            $response['message'] = $ex->getMessage();
        } catch (NotEnoughCoinsException $ex){
            $response['message'] = "You don't have enough coins.";
        }
        
        return new JsonResponse($response);
    }
    
    /**
     * @Route("/last-bets", name="game_roulette_last_bets")
     */
    public function lastBetsAction(){
        
        $user = $this->getUser();
        
        $bets = $this->getDoctrine()->getRepository(RoulleteEntity::class)->getLastResults($user);
        
        return $this->render('roulette/lastBets.html.twig', array(
           'bets' =>  $bets
        ));
        
    }
    
}
