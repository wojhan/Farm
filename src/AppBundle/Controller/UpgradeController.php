<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Upgrade;
use AppBundle\Entity\UpgradeCategory;
use AppBundle\Entity\UpgradeLevel;
use AppBundle\Exception\ForbiddenActionException;
use AppBundle\Exception\ItemNotFoundException;
use AppBundle\Exception\ItemUnlockedException;
use AppBundle\Exception\NotEnoughCoinsException;
use AppBundle\Util\CoinManager;
use AppBundle\Util\UpgradeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game/upgrade-farm")
 * @Security("has_role('ROLE_USER')")
 */
class UpgradeController extends Controller{
    
    /**
     * @Route("/", name="game_upgrade_farm")
     */
    public function indexAction(){
        
        $categories = $this->getDoctrine()->getRepository(UpgradeCategory::class)->findAll();
        
        $user = $this->getUser();
        $upgrades = $user->getUpgrades();
        
        return $this->render('upgrade/upgradeFarm.html.twig', array(
            'page_title' => 'Upgrade you farm',
            'categories' => $categories,
            'upgrades' => $upgrades
        ));
        
    }
    
    /**
     * @Route("/upgrade/{id}", name="game_upgrade_farm_show")
     */
    public function showAction($id){
        
        $user = $this->getUser();
        $userUpgrades = $this->getDoctrine()->getRepository(Upgrade::class)->getUpgradeForUser($user->getId(), $id);
        
        $renderPath = 'upgrade/upgradeModalUnlocked.html.twig';
        
        if(!$userUpgrades){
            $renderPath = 'upgrade/upgradeModal.html.twig';
        }
        
        $upgrade = $this->getDoctrine()->getRepository(UpgradeLevel::class)->findOneById($id);
                
        return $this->render($renderPath, array(
           'id' => $upgrade->getId(),
           'title' => $upgrade->getCategory()." level ".$upgrade->getLevel(),
           'price' => $upgrade->getPrice(),
           'image' => $upgrade->getImage()
        ));
    }
    
    /**
     * @Route("/buy-upgrade/{id}", name="game_upgrade_farm_buy")
     */
    public function buyUpgradeAction($id, UpgradeManager $um, CoinManager $cm){
        
        $user = $this->getUser();
        
        $messageType = "info";
        $message = 'You have bought an upgrade.';
        
        try{
            $um->buyUpgrade($user, $id, $cm);
            $messageType = "success";
        } catch (ItemNotFoundException $ex) {
            $message = "Upgrade doesn't exist.";
            $messageType = "danger";
        } catch (ItemUnlockedException $ex){
            $message = "Upgrade has been unlocked already,";
        } catch (ForbiddenActionException $ex){
            $message = $ex->getMessage();
        } catch (NotEnoughCoinsException $ex){
            $message = "You don't have enough coins.";
        }
        
        $this->addFlash($messageType, $message);
        
        return $this->redirectToRoute('game_upgrade_farm');
        
    }
    
}
