<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ItemNotFoundException;
use AppBundle\Exception\NotEnoughCoinsException;
use AppBundle\Exception\WrongCategoryException;
use AppBundle\Util\CoinManager;
use AppBundle\Util\ItemManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game/market")
 * @Security("has_role('ROLE_USER')")
 */
class MarketController extends Controller{
            
    /**
     * @Route("/", name="game_market")
     */
    public function indexAction(ItemManager $im){
        
        $user = $this->getUser();
        $items = $im->getVegetables($user);
        
        return $this->render('market/market.html.twig', array(
            'page_title' => 'Market',
            'items' => $items
        ));
        
    }
    
     /**
     * @Route("/sell", name="game_sell_vegetable")
     * @Method("POST")
     */
    public function buySeedAction(CoinManager $cm, ItemManager $im, Request $request){
        $itemName = $request->request->get('name');
        $amount = $request->request->get('amount');
        $user = $this->getUser();
                
        try{
            $im->proceedDisposalTransaction($cm, $user, $itemName, $amount);
            $this->addFlash('success', 'You have just sold '.$amount.' '.$itemName);
        } catch(ItemNotFoundException $ex){
            $this->addFlash('danger', $itemName." doesn't exist.");
        } catch(WrongCategoryException $ex){
            $this->addFlash('danger', 'Wrong way!');
        } catch(\AppBundle\Exception\NotEnoughItemsException $ex){
            $this->addFlash('danger', $ex->getMessage());
        }
        
        return new Response("done");
        
    }

}