<?php

namespace AppBundle\Controller;

use AppBundle\Form\SowType;
use AppBundle\Util\CoinManager;
use AppBundle\Util\FarmManager;
use AppBundle\Util\ItemManager;
use AppBundle\Util\UpgradeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game")
 * @Security("has_role('ROLE_USER')")
 */
class GameController extends Controller{
            
    /**
     * @Route("/", name="game_overview")
     */
    public function overviewAction(CoinManager $cm, ItemManager $im, UpgradeManager $um, FarmManager $fm){
        
        $user = $this->getUser();
        $coins = $cm->getCoins($user);
        $items = $im->getItemsfor($user);
               
        $farmLevel = $um->checkFarmUpgradeLevel($user);
        $fertilizerLevel = $um->checkUpgradeLevel($user, 'Fertilizer');
        
        $fm->updateFertilizer($user);
        
        $form = $this->createForm(SowType::class);
        
        
        return $this->render('game/overview.html.twig', array(
            'page_title' => 'Overview',
            'coins' => $coins,
            'items' => $items,
            'farmLevel' => $farmLevel,
            'fertilizerLevel' => $fertilizerLevel,
            'numCrops' => $fm->countCrops($fm->getActiveCropsForUser($user)),
            'form' => $form->createView()
        ));
        
    }
        
    /**
     * @Route("/get-coins", name="game_get_coins")
     */
    public function getCoinsAction(CoinManager $cm){
       
        $user = $this->getUser();
        
        return new Response('<i class="fa fa-money" aria-hidden="true"></i>  <span class="highlight">'.$cm->getCoins($user)."</span> coins");
        
    }
    
}