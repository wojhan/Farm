<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller{
    
    /**
     * @Route("/", name="page_index")
     */
    public function indexAction(){
        
        
        if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            return $this->redirectToRoute('game_overview');
        }
        
        return $this->render('Page/index.html.twig', array(
            'page_title' => "Home"
        ));
    }
        
    /**
     * @Route("/help", name="page_help")
     */
    public function helpAction(){
        
        return $this->render('Page/help.html.twig', array(
            'page_title' => "Help"
        ));
    }
    
    /**
     * @Route("/faq", name="page_faq")
     */
    public function faqAction(){
        
        return $this->render('Page/faq.html.twig', array(
            'page_title' => "FAQ"
        ));
    }
    
    /**
     * @Route("/contact", name="page_contact")
     */
    public function contactAction(){
        
        return $this->render('Page/contact.html.twig', array(
            'page_title' => "Contact"
        ));
    }
    
}
