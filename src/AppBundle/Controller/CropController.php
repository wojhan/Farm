<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ForbiddenActionException;
use AppBundle\Exception\ItemNotFoundException;
use AppBundle\Exception\LimitReachedException;
use AppBundle\Exception\NotEnoughCoinsException;
use AppBundle\Exception\NotEnoughItemsException;
use AppBundle\Util\CoinManager;
use AppBundle\Util\FarmManager;
use AppBundle\Util\ItemManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game/crops")
 * @Security("has_role('ROLE_USER')")
 */

class CropController extends Controller{
    
    /**
     * @Route("/", name="game_get_crops")
     */
    public function getCropsAction(FarmManager $fm){
          
        $user = $this->getUser();
        $crops = $fm->getActiveCropsForUser($user);
        $fertilizer = $fm->getActiveFertilizerForUser($user);
                
        return $this->render('crops/getCrops.html.twig', array(
            'crops' => $crops,
            'fertilizer' => $fertilizer
        ));
    }
    
    /**
     * @Route("/sow", name="game_sow_seed")
     */
    public function sowAction(Request $request, FarmManager $fm, ItemManager $im){
        $seed = $request->request->get('sow')['seed'];
        $amount = $request->request->get('sow')['amount'];
        $user = $this->getUser();
        
        try{
            $fm->sow($seed, $user, $amount, $im);
        } catch (ItemNotFoundException $ex) {
            $this->addFlash("danger", $seed." doesn't exist");
            return $this->redirectToRoute('game_overview');
        } catch (ForbiddenActionException $ex){
            $this->addFlash("info", "Something went wrong, that action was forbidden.");
            return $this->redirectToRoute('game_overview');
        } catch (NotEnoughItemsException $ex){
            $this->addFlash("danger", "You don't have enough seeds.");
            return $this->redirectToRoute('game_overview');
        } catch(LimitReachedException $ex){
            $this->addFlash("info", $ex->getMessage());
            return $this->redirectToRoute('game_overview');
        }
        
        $this->addFlash("success", "You have just sowed seeds");
        return $this->redirectToRoute('game_overview');
        
    }
    
    /**
     * @Route("/harvest", name="game_harvest")
     */
    public function harvestAction(FarmManager $fm){
        
        $user = $this->getUser();
               
        try{
            $fm->harvest($user);
        } catch(ItemNotFoundException $ex){
            $this->addFlash("info", "Nothing to harvest.");
            return $this->redirectToRoute("game_overview");
        }
        
        $this->addFlash("success", "You have harvested.");
        
        return $this->redirectToRoute("game_overview");
        
    }
    
    /**
     * @Route("/fertilize", name="game_fertilize")
     */
    public function fertilizeAction(FarmManager $fm, CoinManager $cm){
        
        $user = $this->getUser();
        $price = 500;
        
        try{
            $fm->fertilize($user, $price, $cm);
            $this->addFlash('success', 'You have fertilized your farm.');
        } catch (AppBundle\Exception\ForbiddenActionException $ex) {
            $this->addFlash('danger', $ex->getMessage());
        } catch (NotEnoughCoinsException $ex){
            $this->addFlash('danger', "You don't have enough coins.");
        } catch (LimitReachedException $ex){
            $this->addFlash("info", "You can't use fertilizer more than once.");
        }
        
        return $this->redirectToRoute('game_overview');
        
    }
    
}
