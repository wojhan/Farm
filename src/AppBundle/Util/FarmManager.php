<?php

namespace AppBundle\Util;

use AppBundle\Entity\Crop;
use AppBundle\Entity\Item;
use AppBundle\Exception\ForbiddenActionException;
use AppBundle\Exception\ItemNotFoundException;
use AppBundle\Exception\LimitReachedException;
use AppBundle\Exception\NotEnoughItemsException;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FarmManager{
    
    private $em;
    private $um;
    private $container;
    
    private $config;
    
    public function __construct(EntityManager $em, UpgradeManager $um, ContainerInterface $container){
        $this->em = $em;
        $this->um = $um;
        $this->container = $container;
        $this->config = $this->loadConfig();
    }
    
    private function loadConfig(){
        return $this->container->get('craue_config');
    }
    
    /**
     * It allows to get user's crops which haven't been harvested yet.
     * @param User $user
     * @return array|null
     */
    public function getActiveCropsForUser($user){
        $userId = $user->getId();
        
        return $this->em->getRepository(Crop::class)
                ->getActiveCropsForUser($userId);
                
    }
    
    /**
     * 
     * @param array $crops
     * @return integer Amount of crops
     */
    public function countCrops($crops){
        $numCrops = 0;
        foreach($crops as $crop){
            $numCrops += $crop['amount'];
        }
        
        return $numCrops;
    }
    
     /**
     * Getting user's crops where $date is after "finish at" time.
     * @param datetime $date
     * @param User $user
     * @return array|null
     */
    public function getReadyToHarvestCropsForUser($date, $user){
        $userId = $user->getId();
        
        return $this->em->getRepository(Crop::class)
                ->getReadyToHarvestCropsForUser($date, $userId);
    }
    
    /**
     * Harvests mature crops, setting correct status in database.
     * @param User $user
     * @throws ItemNotFoundException
     */
    public function harvest($user){
        $date = new DateTime();
        $crops = $this->getReadyToHarvestCropsForUser($date, $user);
        
        if(!$crops){
            throw new ItemNotFoundException("There's nothing to harvest on ".$user->getUsername()."'s farm");
        }
        
        $repo = $this->em->getRepository(Item::class);
        
        foreach($crops as $crop){
            $seed = $repo->findOneById($crop['id']);
            $vegetableName = explode(' ',$seed->getName())[0];
            $vegetable = $repo->findOneByName($vegetableName);
            $vegetableId = $vegetable->getId();
            $user->addVegetable($vegetableId, round($crop['amount'] * $crop['multiplier']));
        }
        
        $this->em->flush();
        
        $cropRepo = $this->em->getRepository(Crop::class);
        
        $cropRepo->updateHarvestedForUser($date, $user->getId());
        
    }
    
    private function generateNumber($min, $max){
        return rand($min, $max);
    }
    
    private function generateCropMultiplier($user){
        
        switch($this->um->checkUpgradeLevel($user, 'Fertilizer') && $this->getActiveFertilizerForUser($user)){
            case 0:
            default:
                return 1;
            case 1:
                return $this->generateNumber(20, 25)/10;
            case 2:
                return $this->generateNumber(30, 40)/10;
        }
        
        return 1;

    }
    
    /**
     * 
     * @param Item $seed
     * @param User $user
     * @param integer $amount
     * @param ItemManager $im
     * @throws ItemNotFoundException
     * @throws LimitReachedException
     * @throws ForbiddenActionException
     * @throws NotEnoughItemsException
     */
    public function sow($seed, $user, $amount, ItemManager $im){
        
        $finishAt = new DateTime("now");
        $item = $this->em->getRepository(Item::class)->findOneById($seed);
        $farmLevel = $this->um->checkFarmUpgradeLevel($user);
        $farmCapacity = $this->config->get("farm_capacity_level_$farmLevel");
        
        if(!$item){
            throw new ItemNotFoundException($seed. " doesn't exist");
        }
        if($amount > $this->config->get('sow_limit')){
            throw new LimitReachedException("Sow limit has been reached");
        }
        if($this->countCrops($this->getActiveCropsForUser($user)) + $amount > $farmCapacity){
            throw new LimitReachedException("Farm doesn't let to sow more seeds.");
        }
        if(!$im->validateItemCategory($item, 'Seed') || !$im->validateUserItems($user, $item)){
            throw new ForbiddenActionException("Action is forbidden.");
        }
        
        $duration = $item->getDuration();
        $multiplier = $this->generateCropMultiplier($user);
        
        $crop = new Crop();
        $crop->setAmount($amount);
        $crop->setFinishAt($finishAt->modify("+$duration minutes"));
        $crop->setItem($item);
        $crop->setUser($user);
        $crop->setMultiplier($multiplier);
        
        try{
            $user->removeVegetable($item->getId(), $amount);
            $this->em->persist($crop);
            $this->em->flush();
        } catch (NotEnoughItemsException $ex) {
            throw $ex;
        }               
        
    }
    
    /**
     * 
     * @param User $user
     * @return array|null An array with active fertilizer for the user.
     */
    public function getActiveFertilizerForUser($user){
        
        $userId = $user->getId();
        
        return $this->em->getRepository(\AppBundle\Entity\Fertilizer::class)
                ->getActiveFertilizerForUser($userId);
    }
    
    /**
     * 
     * @param User $user
     * @param integer $price
     * @param \AppBundle\Util\CoinManager $cm
     * @throws ForbiddenActionException
     * @throws LimitReachedException
     * @throws NotEnoughCoinsException
     */
    public function fertilize($user, $price, CoinManager $cm){
        
        if($this->um->checkFarmUpgradeLevel($user) == 0){
            throw new ForbiddenActionException("At least farm level 1 is required to use fertilizer.");
        }
        
        if($this->um->checkUpgradeLevel($user, 'Fertilizer') == 0){
            throw new ForbiddenActionException("Fertilizer hasn't been unlocked yet.");
        }
        
        if($this->getActiveFertilizerForUser($user)){
            throw new LimitReachedException("Only one fertilizer at once permitted.");
        }
        
        $finishAt = new \DateTime();
        
        $fertilizer = new \AppBundle\Entity\Fertilizer();
        $fertilizer->setFinishAt($finishAt->modify("+30 minutes"));
        $fertilizer->setUser($user);
        
        try{
            $cm->removeCoins($user, $price, 'User has fertilized his farm.');
            $this->em->persist($fertilizer);
            $this->em->flush();
        } catch (\AppBundle\Exception\NotEnoughCoinsException $ex) {
            throw $ex;
        } catch (ForbiddenActionException $ex){
            throw $ex;
        }
       
    }
    
    public function updateFertilizer($user){
        
        $this->em->getRepository(\AppBundle\Entity\Fertilizer::class)
                ->updateFertilizerForUser($user->getId());
        
    }
            
}