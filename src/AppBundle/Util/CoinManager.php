<?php

namespace AppBundle\Util;

use AppBundle\Entity\Coin;
use AppBundle\Exception\ForbiddenActionException;
use AppBundle\Exception\NotEnoughCoinsException;
use DateTime;
use Doctrine\ORM\EntityManager;

class CoinManager{
    
    private $em;
    
    public function __construct(EntityManager $em){
        $this->em = $em;
    }
    
    /**
     * 
     * @param User $user
     * @param integer $amount 
     * @param string $action A short description, reason.
     */
    public function addCoins($user, $amount, $action){
       
        $coin = new Coin();
        $coin->setDate(new DateTime('now'));
        $coin->setUser($user);
        $coin->setAmount($amount);
        $coin->setAction($action);
        
        $this->em->persist($coin);
        $this->em->flush();
    }
    
    /**
     * 
     * @param User $user
     * @param integer $amount
     * @param string $action A short description, reason.
     * @throws NotEnoughCoinsException
     * @throws ForbiddenActionException
     */
    public function removeCoins($user, $amount, $action){
        
        $userBalance = $this->getCoins($user);
        if($userBalance < $amount){
            throw new NotEnoughCoinsException($user->getUsername(). "doesn't have enough coins to be removed.");
        }
        if($amount < 0){
            throw new ForbiddenActionException("Negative amount of coins.");
        }
        
        $this->addCoins($user, -($amount), $action);
        
    }
    
    /**
     * 
     * @param User $user
     * @return integer Amount of coins the user has.
     */
    public function getCoins($user){
        
        $coins = $this->em->getRepository(Coin::class)->findByUser($user);
        
        $totalCoins = 0;
        foreach($coins as $coin){
            $totalCoins += $coin->getAmount();
        }
        
        return $totalCoins;
    }
    
}