<?php

namespace AppBundle\Util;

use AppBundle\Entity\Item;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\UserItem;
use AppBundle\Exception\ItemNotFoundException;
use AppBundle\Exception\NotEnoughCoinsException;
use AppBundle\Exception\WrongCategoryException;
use DateTime;
use Doctrine\ORM\EntityManager;

class ItemManager{
    
    const SEED_ID = 1;
    const VEGETABLE_ID = 2;
    
    private $em;
    
    public function __construct(EntityManager $em){
        $this->em = $em;
    }
    
    /**
     * 
     * @param User $user
     */
    public function addStartingItems($user){
        
        $itemRepo = $this->em->getRepository(Item::class);
        
        $carrotSeed = $itemRepo->findOneByName('Carrot seed'); 
        $carrot = $itemRepo->findOneByName('Carrot');
        
        $userItem1 = new UserItem();
        $userItem1->setItem($carrotSeed);
        $userItem1->setUser($user);
        
        $userItem2 = new UserItem();
        $userItem2->setItem($carrot);
        $userItem2->setUser($user);
        
        $this->em->persist($userItem1);
        $this->em->persist($userItem2);
        $this->em->flush();
    }
    
    /**
     * 
     * @param User $user
     * @return array|null An array seeds available for the user.
     */
    public function getSeeds($user){
               
        $userId = $user->getId();
                
       return $this->em->getRepository(UserItem::class)
               ->getItemsByCategory($userId, $this::SEED_ID);

    }
    
    /**
     * 
     * @param User $user
     * @return array|null An array vegetables available for the user.
     */
    public function getVegetables($user){
        
        $userId = $user->getId();
        
        return $this->em->getRepository(UserItem::class)
                ->getItemsByCategory($userId, $this::VEGETABLE_ID);
    }
    
    /**
     * 
     * @param User $user
     * @return array|null An array items available for the user.
     */
    public function getItemsfor($user){
        
        $userId = $user->getId();
        
        return $this->em->getRepository(UserItem::class)
                ->getItemsFor($userId);
    }
    
    /**
     * 
     * @param User $user
     * @param Item $item
     * @return array|null An Array specific item for the user
     */
    public function getItemFor($user, $item){
        
        $userId = $user->getId();
        $itemId = $item->getId();
        
        return $this->em->getRepository(UserItem::class)
                ->getItemFor($userId, $itemId);
        
    }
    
    /**
     * Checks if the user has right to use the item.
     * @param User $user
     * @param Item $item
     * @return boolean true if the user is permitted to have the item.
     */
    public function validateUserItems($user, $item){
        $unlockedItems = $this->getItemsfor($user);
                
        foreach($unlockedItems as $unlockedItem){
            if($unlockedItem['id'] == $item->getId()){
               return true;
            }
        }
        
        return false;
    }
    
    /**
     * Checks if the item has set the category.
     * @param Item $item
     * @param string $categoryName
     * @return boolean true if item is in given category.
     */
    public function validateItemCategory($item, $categoryName){
        if($item->getCategory() == $categoryName){
            return true;
        }
        
        return false;
    }
    
    public function proceedShoppingTransaction(CoinManager $cm, $user, $itemName, $amount){
        
        $item = $this->em->getRepository(Item::class)->findOneByName($itemName);
        
        $price = $item->getPrice() * $amount;
        
        $transaction = new Transaction();
        $transaction->newTransaction($user, $item, $price, $amount);
        
        if(!$item){
            throw new ItemNotFoundException($itemName." doesn't exists.");
        }
                    
        if(!$this->validateItemCategory($item, 'Seed')){
            throw new WrongCategoryException("Only seeds may be bought.");
        }
        
        try{
            $cm->removeCoins($user, $price, $user->getUsername()." has bought".$amount." ".$itemName);
            $user->addVegetable($item->getId(), $amount);
        } catch (NotEnoughCoinsException $ex) {
            throw $ex;
        }
                               
        return $this->proceedTransaction($transaction, $user, $item);
    }
    
    public function proceedDisposalTransaction(CoinManager $cm, $user, $itemName, $amount){
        
        $item = $this->em->getRepository(Item::class)->findOneByName($itemName);
        
        $price = $item->getPrice() * $amount;
        
        $transaction = new Transaction();
        $transaction->newTransaction($user, $item, $price, $amount);
        
        if(!$item){
            throw new ItemNotFoundException($itemName." doesn't exists.");
        }
                    
        if(!$this->validateItemCategory($item, 'Vegetable')){
            throw new WrongCategoryException("Only vegetables may be sold.");
        }
        
        try{
            $user->removeVegetable($item->getId(), $amount);
            $cm->addCoins($user, $price, "User has sold $amount $itemName");
        } catch (\AppBundle\Exception\NotEnoughItemsException $ex) {
            throw $ex;
        }
        
        return $this->proceedTransaction($transaction, $user, $item);
    }
    //to do proceed shopping/disposal transactions, validate itemcategory for both cases, validateitem
    public function proceedTransaction($transaction, $user, $item){
            
          if(!$this->validateUserItems($user, $item)){
              return false;
          }
          
          $this->em->persist($transaction);
          $this->em->flush();
          
          return true;
    }
    
}