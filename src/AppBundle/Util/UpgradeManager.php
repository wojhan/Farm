<?php

namespace AppBundle\Util;

use Doctrine\ORM\EntityManager;

class UpgradeManager{
    
    private $em;
    
    public function __construct(EntityManager $em){
        $this->em = $em;
    }
    
    public function checkFarmUpgradeLevel($user){
        return $this->checkUpgradeLevel($user, 'Farm');
    }
    
    public function checkUpgradeLevel($user, $upgradeCategoryName){
        
        $upgrades = $user->getUpgrades();
        $level = 0;
        
        foreach($upgrades as $upgrade){
            $upgradeCategory = $upgrade->getCategory();
            $upgradeLevel = $upgrade->getLevel()->getLevel();
            
            if($upgradeCategory->getName() == $upgradeCategoryName && $upgradeLevel > $level){
                $level = $upgradeLevel;
            }
        }
        
        return $level;
       
    }
    
    public function isUserHasUpgrade($user, $upgradeId){
        
        $userUpgrades = $this->em
                ->getRepository(\AppBundle\Entity\Upgrade::class)
                ->getUpgradeForUser($user->getId(), $upgradeId);
        
        if(!$userUpgrades){
            return false;
        }
        
        return true;
        
    }
    
    private function unlockCrops($user, $level){
        $repo = $this->em->getRepository(\AppBundle\Entity\Item::class);
        switch($level){
            case 1:
                $tomatoSeed = $repo->findOneByName('Tomato seed');
                $tomato = $repo->findOneByName('Tomato');
                
                $userItem1 = new \AppBundle\Entity\UserItem();
                $userItem1->setItem($tomatoSeed);
                $userItem1->setUser($user);
                
                $userItem2 = new \AppBundle\Entity\UserItem();
                $userItem2->setItem($tomato);
                $userItem2->setUser($user);
                
                break;
            case 2:
                $asparagusSeed = $repo->findOneByName('Asparagus seed');
                $asparagus = $repo->findOneByName('Asparagus');
                
                $userItem1 = new \AppBundle\Entity\UserItem();
                $userItem1->setItem($asparagusSeed);
                $userItem1->setUser($user);
                
                $userItem2 = new \AppBundle\Entity\UserItem();
                $userItem2->setItem($asparagus);
                $userItem2->setUser($user);
                
                break;
        }
        
        $this->em->persist($userItem1);
        $this->em->persist($userItem2);
    }
    
    public function buyUpgrade($user, $levelId, CoinManager $cm){
        
        $upgradeLevel = $this->em->getRepository(\AppBundle\Entity\UpgradeLevel::class)->findOneById($levelId);
        $price = $upgradeLevel->getPrice();
        $level = $upgradeLevel->getLevel();
        $category = $upgradeLevel->getCategory();
        
        $upgrade = new \AppBundle\Entity\Upgrade();
        $upgrade->setCategory($category);
        $upgrade->setLevel($upgradeLevel);
        $upgrade->setName($category->getName()." level ".$level);
        $upgrade->setUser($user);
        
        if(!$upgradeLevel){
            throw new \AppBundle\Exception\ItemNotFoundException("Upgrade doesn't exist.");
        }
        
        if($this->checkUpgradeLevel($user, $category->getName()) < $level - 1){
            throw new \AppBundle\Exception\ForbiddenActionException("You have to unlock upgrades level by level!");
        }
        
        if($this->isUserHasUpgrade($user, $levelId)){
            throw new \AppBundle\Exception\ItemUnlockedException("Upgrade has been unlocked already.");
        }
        
        try{
            $cm->removeCoins($user, $price, "User has bought an upgrade: ".$upgrade->getName());
            if($category->getName() == 'Crops'){
                $this->unlockCrops($user, $level);
            }
            $this->em->persist($upgrade);
            $this->em->flush();
        } catch (\AppBundle\Exception\ForbiddenActionException $ex) {
            throw $ex;
        } catch(\AppBundle\Exception\NotEnoughCoinsException $ex){
            throw $ex;
        }
                
    }
        
}