<?php

namespace AppBundle\Util;

use AppBundle\Entity\Roulette as RouletteEntity;
use AppBundle\Exception\ForbiddenActionException;
use AppBundle\Exception\NotEnoughCoinsException;
use Doctrine\ORM\EntityManager;

class Roulette {
    
    private $numOrder;
    
    private $numRed;
    
    private $numGreen;
    
    private $numBlack;
    
    private $em;
    
    public function __construct(EntityManager $em) {
        $this->numOrder = [0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26];
        $this->numRed = [32,19,21,25,34,27,36,30,23,5,16,1,14,9,18,7,12,3];
        $this->numBlack = [15,4,2,17,6,13,11,8,10,24,33,20,31,22,29,28,35,26];
        $this->numGreen = [0];
        
        $this->em = $em;
    }
    
    private function generateNum(){
        return rand(0, 36);
    }
    
    private function checkBet($betType, $num){
  
        switch($betType){
            case 1:
                if(in_array($num, $this->numRed)){
                    return true;
                }
                break;
            case 2:
                if(in_array($num, $this->numGreen)){
                    return true;
                }
                break;
            case 3:
                if(in_array($num, $this->numBlack)){
                    return true;
                }
                break;
            case 4:
                if($num >= 1 && $num <=12){
                    return true;
                }
                break;
            case 5:
                if($num >=13 && $num <= 24){
                    return true;
                }
                break;
            case 6:
                if($num >=25){
                    return true;
                }
                break;
        }
        
        return false;
    }
    
    /**
     * 
     * @param integer $bet betType, 1 - red, 2 - green, 3 - black, 4 - 1 to 12, 5 - 13 to 24, 6 - 25 to 36
     * @param integer $coins
     * @param User $user
     * @param \AppBundle\Util\CoinManager $cm
     */
    private function givePrizeForUser($bet, $coins, $user, CoinManager $cm){
        switch($bet){
            case 1:
            case 2:
            case 3:
                $cm->addCoins($user, $coins * 2, "User has won a bet");
                break;
            case 4:
            case 5:
            case 6:
                $cm->addCoin($user, $coins * 3, "User has won a bet");
                break;
        }
    }
    
    /**
     * 
     * @param User $user
     * @param integer $betType
     * @param integer $coins
     * @param \AppBundle\Util\CoinManager $cm
     * @return integer A drawn number
     * @throws ForbiddenActionException
     * @throws NotEnoughCoinsException
     */
    public function bet($user, $betType, $coins, CoinManager $cm){
        
        $number = $this->generateNum();
        
        $bet = new RouletteEntity();
        $bet->setUser($user);
        $bet->setBet($betType);
        $bet->setAmount($coins);
        $bet->setStatus(false);
        
        if($betType > 6){
            throw new ForbiddenActionException("Forbidden action, bet doesn't exist");
        }
        
        try{
            $cm->removeCoins($user, $coins, 'User has bet');
            if($this->checkBet($betType, $number)){
                $bet->setStatus(true);
                $this->givePrizeForUser($betType, $coins, $user, $cm);
            }
            $this->em->persist($bet);
            $this->em->flush();
        } catch (NotEnoughCoinsException $ex) {
            throw $ex;
        } catch (ForbiddenActionException $ex){
            throw $ex;
        }
        
        return $number;
    }
    
}
