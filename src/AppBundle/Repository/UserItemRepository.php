<?php

namespace AppBundle\Repository;


class UserItemRepository extends \Doctrine\ORM\EntityRepository{
    
    /**
     * 
     * @param integer $userId
     * @param integer $categoryId
     * @return array|null
     */
    public function getItemsByCategory($userId, $categoryId){
        $query = 'SELECT item.id, item.imageUrl, item.name, item.price '
                . 'FROM AppBundle:UserItem ui '
                . 'INNER JOIN AppBundle:Item item WITH item.id = ui.item '
                . 'INNER JOIN AppBundle:User user WITH user.id = ui.user '
                . 'WHERE item.category = :categoryId AND user.id = :userId '
                . 'ORDER BY item.category, item.id';
        
        return $this->getEntityManager()
                ->createQuery($query)
                ->setParameter('categoryId', $categoryId)
                ->setParameter('userId', $userId)
                ->getResult();
    }
    
    /**
     * 
     * @param integer $userId
     * @return array|null
     */
    public function getItemsFor($userId){
        $query = 'SELECT item.id, item.imageUrl, item.name, item.price '
                . 'FROM AppBundle:UserItem ui '
                . 'INNER JOIN AppBundle:Item item WITH item.id = ui.item '
                . 'INNER JOIN AppBundle:User user WITH user.id = ui.user '
                . 'WHERE user.id = :userId '
                . 'ORDER BY item.category, item.id';
        
        return $this->getEntityManager()
                ->createQuery($query)
                ->setParameter('userId', $userId)
                ->getResult();
    }
    
    /**
     * 
     * @param integer $userId
     * @param integer $itemId
     * @return array|null
     */
    public function getItemFor($userId, $itemId){
        $query = 'SELECT item.id, item.imageUrl, item.name, item.price '
                . 'FROM AppBundle:UserItem ui '
                . 'INNER JOIN AppBundle:Item item With item.id = ui.item '
                . 'INNER JOIN AppBundle:User user WITH user.id = ui.user '
                . 'WHERE user.id = :userId AND item.id = :itemId '
                . 'ORDER BY item.category, item.id';
        
        return $this->getEntityManager()
                ->createQuery($query)
                ->setParameter('userId', $userId)
                ->setParameter('itemId', $itemId)
                ->getResult();
    }
}
