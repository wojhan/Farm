<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class SowType extends AbstractType{
    
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options){        
        $builder
                ->add('seed', EntityType::class, array(
                    'class' => 'AppBundle:UserItem',
                    'query_builder' => function (EntityRepository $er){
                        $user = $this->container->get('security.token_storage')->getToken()->getUser();
                        
                        return $er->createQueryBuilder('ui')
                                ->addSelect('item')
                                ->innerJoin('ui.item', 'item')
                                ->innerJoin('ui.user', 'user')
                                ->where('item.category = :categoryId')
                                ->andWhere('user.id = :userId')
                                ->setParameter('userId', $user->getId())
                                ->setParameter('categoryId', 1);
                    },
                    'choice_label' => 'item.name',
                    'choice_value' => 'item.id'
                ))
                ->add('amount', NumberType::class);
    }
    
}