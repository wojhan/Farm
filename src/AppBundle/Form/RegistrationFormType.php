<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;

class RegistrationFormType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, array('label' => 'form.name', 'translation_domain' => 'FOSUserBundle'))
                ->add('btc_address', TextType::class, array('label' => 'form.btc_address', 'translation_domain' => 'FOSUserBundle'));
    }
    
    public function getParent() {
        return BaseRegistrationFormType::class;
    }
}