<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UpgradeCategory
 *
 * @ORM\Table(name="upgrade_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UpgradeCategoryRepository")
 */
class UpgradeCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Upgrade", mappedBy="category")
     */
    private $upgrades;
    
    /**
     * @ORM\OneToMany(targetEntity="UpgradeLevel", mappedBy="category")
     */
    private $levels;
    
    public function __construct() {
        $this->upgrades = new \Doctrine\Common\Collections\ArrayCollection();
        $this->levels = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
        return $this->name;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }

    public function getUpgrades() {
        return $this->upgrades;
    }

    public function addUpgrade($upgrade){
        $this->upgrades[] = $upgrade;
        
        return $this;
    }
    
    public function getLevels(){
        return $this->levels;
    }
    
    public function addLevel($level){
        $this->levels[] = $level;
        
        return $this;
    }
}

