<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Upgrade
 *
 * @ORM\Table(name="upgrade")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UpgradeRepository")
 */
class Upgrade
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="upgrades")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="UpgradeCategory", inversedBy="upgrades")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @ORM\ManyToOne(targetEntity="UpgradeLevel", inversedBy="upgrades")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id")
     */
    private $level;
    
    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    public function __construct() {
        $this->date = new \DateTime();
    }

    public function __toString(){
        return $this->name;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }

    public function getDate() {
        return $this->date;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function getUser() {
        return $this->user;
    }

    public function getCategory() {
        return $this->category;
    }
    
    public function setUser($user) {
        $this->user = $user;
    }

    public function setCategory($category) {
        $this->category = $category;
    }
    
    public function getLevel() {
        return $this->level;
    }

    public function setLevel($level) {
        $this->level = $level;
    }

}

