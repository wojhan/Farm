<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Crop
 *
 * @ORM\Table(name="crop")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CropRepository")
 */
class Crop
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="sowed_at", type="datetime")
     */
    private $sowedAt;
    
    /**
     * @ORM\Column(name="finish_at", type="datetime")
     */
    private $finishAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="crops")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="crops")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;
    
    /**
     * @ORM\Column(name="multiplier", type="float")
     */
    private $multiplier;
    
    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;
    
    public function __construct() {
        $this->sowedAt = new \DateTime('now');
        $this->active = true;
        $this->multiplier = 1;
    }
    
    public function __toString() {
        return "no clue";
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getSowedAt() {
        return $this->sowedAt;
    }
    
    public function getFinishAt() {
        return $this->finishAt;
    }

    public function getItem() {
        return $this->item;
    }

    public function getUser() {
        return $this->user;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getActive() {
        return $this->active;
    }

    public function setSowedAt($sowedAt) {
        $this->sowedAt = $sowedAt;
    }

    public function setFinishAt($finishAt) {
        $this->finishAt = $finishAt;
    }

    public function setItem($item) {
        $this->item = $item;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
    }

    public function setActive($active) {
        $this->active = $active;
    }
    
    public function getMultiplier() {
        return $this->multiplier;
    }

    public function setMultiplier($multiplier) {
        $this->multiplier = $multiplier;
    }



}

