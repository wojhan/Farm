<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemRepository")
 */
class Item
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     */
    private $imageUrl;
    
    /**
     * @ORM\Column(name="price", type="integer")
     */
    private $price;
    
    /**
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;
    
    /**
     * @ORM\ManyToOne(targetEntity="ItemCategory", inversedBy="items")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @ORM\OneToMany(targetEntity="UserItem", mappedBy="item")
     */
    private $users;
    
    /**
     * @ORM\OneToMany(targetEntity="Crop", mappedBy="item")
     */
    private $crops;
    
    
    /**
     * Get id
     *
     * @return int
     */
    
    public function __construct() {
        $this->transactions = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->items = new ArrayCollection();
    }
    
    public function __toString(){
        return $this->name;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }

    public function getImageUrl() {
        return $this->imageUrl;
    }

    public function getPrice() {
        return $this->price;
    }
    
    public function getDuration() {
        return $this->duration;
    }

    public function setDuration($duration) {
        $this->duration = $duration;
    }

    
    public function setName($name) {
        $this->name = $name;
    }

    public function setImageUrl($imageUrl) {
        $this->imageUrl = $imageUrl;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

        
    public function getCategory() {
        return $this->category;
    }

    public function setCategory($category) {
        $this->category = $category;
    }
    
    public function getUsers() {
        return $this->users;
    }
    
    public function getCrops(){
        return $this->crops;
    }
    
    
}

