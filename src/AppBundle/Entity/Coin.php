<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="coin")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CoinRepository")
 */
class Coin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="coins")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $amount;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $action;
    
    public function __construct(){
        $this->date = new \DateTime();
    }
    
    public function __toString() {
        return $this->amount.' : '.$this->action;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getDate() {
        return $this->date;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getAction() {
        return $this->action;
    }
    
    public function getUser() {
        return $this->user;
    }
    
    public function setDate($date) {
        $this->date = $date;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
    }

    public function setAction($action) {
        $this->action = $action;
    }
    
    public function setUser($user) {
        $this->user = $user;
    }
}