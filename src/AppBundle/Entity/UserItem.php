<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserItem
 *
 * @ORM\Table(name="userItem")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserItemRepository")
 */
class UserItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
        
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="items")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="users")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $item;
    
    public function __toString() {
        return "ech";
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUser() {
        return $this->user;
    }

    public function getItem() {
        return $this->item;
    }
    
    public function setUser($user) {
        $this->user = $user;
    }

    public function setItem($item) {
        $this->item = $item;
    }




    
}