<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * UpgradeLevel
 *
 * @ORM\Table(name="upgrade_level")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UpgradeLevelRepository")
 * @Vich\Uploadable
 */
class UpgradeLevel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="level", type="integer")
     */
    private $level;
    
    /**
     * @ORM\Column(name="price", type="integer")
     */
    private $price;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @Vich\UploadableField(mapping="upgrade_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    
    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="UpgradeCategory", inversedBy="levels")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\OneToMany(targetEntity="Upgrade", mappedBy="level")
     */
    private $upgrades;
    
    public function __construct() {
        $this->upgrades = new \Doctrine\Common\Collections\ArrayCollection();
        
        $this->createdAt = new \DateTime();
    }
    
    public function __toString() {
        return (string)$this->level;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getLevel() {
        return $this->level;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setLevel($level) {
        $this->level = $level;
    }

    public function setPrice($price) {
        $this->price = $price;
    }
    
    public function getCategory() {
        return $this->category;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    
    public function getUpgrades() {
        return $this->upgrades;
    }

    public function addUpgrade($upgrade){
        $this->upgrades[] = $upgrade;
        
        return $this;
    }
    
    public function setImageFile(File $image = null){
        $this->imageFile = $image;
        
        if($image){
            // stupid but it's required to event listeners.
            $this->createdAt = new \DateTime();
        }
    }
    
     public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
    
    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }
    
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

   
}

