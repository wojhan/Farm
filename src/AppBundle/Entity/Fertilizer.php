<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fertilizer
 *
 * @ORM\Table(name="fertilizer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FertilizerRepository")
 */
class Fertilizer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="finish_at", type="datetime")
     */
    private $finishAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fertilizerItems")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;
    
    public function __construct() {
        $this->status = true;
        $this->createdAt = new \DateTime();
    }
    
    public function __toString() {
        return (string)$this->id;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getFinishAt() {
        return $this->finishAt;
    }

    public function getUser() {
        return $this->user;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    public function setFinishAt($finishAt) {
        $this->finishAt = $finishAt;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

}

