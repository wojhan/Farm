<?php

namespace AppBundle\Entity;

use AppBundle\Exception\NotEnoughItemsException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $btcAddress;
    
    /**
     * @ORM\OneToMany(targetEntity="Coin", mappedBy="user")
     */
    protected $coins;
    
    /**
     * @ORM\OneToMany(targetEntity="UserItem", mappedBy="user")
     */
    protected $items;
    
    /**
     * @ORM\Column(type="array")
     */
    protected $vegetables;
    
    /**
     * @ORM\OneToMany(targetEntity="Upgrade", mappedBy="user")
     */
    protected $upgrades;
    
    /**
     * @ORM\OneToMany(targetEntity="Crop", mappedBy="user")
     */
    protected $crops;
    
    /**
     * @ORM\OneToMany(targetEntity="Roulette", mappedBy="user")
     */
    protected $bets;
    
    /**
     * @ORM\OneToMany(targetEntity="Fertilizer", mappedBy="user")
     */
    protected $fertilizerItems;
    
    public function __construct() {
        parent::__construct();
        $this->coins = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->crops = new ArrayCollection();
        $this->bets = new ArrayCollection();
        $this->fertilizerItems = new ArrayCollection();
        
        $this->vegetables =  array();
    }

    public function getName() {
        return $this->name;
    }

    public function getBtcAddress() {
        return $this->btcAddress;
    }
    
    public function getCoins() {
        return $this->coins;
    }
    
    
    public function setName($name) {
        $this->name = $name;
    }

    public function setBtcAddress($btcAddress) {
        $this->btcAddress = $btcAddress;
    }
    
    public function getItems() {
        return $this->items;
    }
    
    public function setItems($items) {
        $this->items = $items;
    }
    
    public function addItem($item){
        $this->items[] = $item;
        
        return $this;
    }
    
    public function getUpgrades() {
        return $this->upgrades;
    }
    
    public function addUpgrade($upgrade){
        $this->upgrades[] = $upgrade;
        
        return $this;
    }
    
    public function getCrops(){
        return $this->crops;
    }
    
    public function addCrop($crop){
        $this->crops[] = $crop;
        
        return $this;
    }
        
    public function getVegetables(){        
        return $this->vegetables;
    }
    
    public function setVegetables($vegetables) {
        $this->vegetables = $vegetables;
    }
    
    public function getBets(){
        return $this->bets;
    }
    
    public function setBets($bets){
        $this->bets = $bets;
    }
    
    public function addBet($bet){
        $this->bets[] = $bet;
        
        return $this;
    }

        
    public function addVegetable($vegetableId, $amount = 0){
        if(!array_key_exists($vegetableId, $this->vegetables)){
            $this->vegetables[$vegetableId] = $amount;
        } else{
            $this->vegetables[$vegetableId] += $amount;
        }
        
        return 'ok';
    }
    
    public function removeVegetable($vegetableId, $amount = 0){
        if(array_key_exists($vegetableId, $this->vegetables)){
            if($amount > $this->vegetables[$vegetableId]){
                throw new NotEnoughItemsException("Tried remove too many vegetables.");
            }
            $this->vegetables[$vegetableId] -= $amount;
        }
    }
    
}