<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roulette
 *
 * @ORM\Table(name="roulette")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RouletteRepository")
 */
class Roulette
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="bet_at", type="datetime")
     */
    private $betAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bets")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\Column(name="bet", type="integer")
     */
    private $bet;

    /**
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;
    
    /**
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    
    public function __construct(){
        $this->betAt = new \DateTime();
    }
    
    public function __toString() {
        return 'no clue yet';
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getBetAt() {
        return $this->betAt;
    }

    public function getUser() {
        return $this->user;
    }

    public function getBet() {
        return $this->bet;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setBetAt($betAt) {
        $this->betAt = $betAt;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setBet($bet) {
        $this->bet = $bet;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
    }

    public function setStatus($status) {
        $this->status = $status;
    }


}

