<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;
    
    /**
     * @ORM\Column(name="price", type="integer")
     */
    private $price;
    
    /**
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * Get id
     *
     * @return int
     */
    
    public function __construct(){
        $date = new \DateTime();
        $this->setDate($date);
    }
    
    public function __toString() {
        return (string)$this->id;
    }
        
    public function getId()
    {
        return $this->id;
    }
    
    public function getDate() {
        return $this->date;
    }

    public function getUser() {
        return $this->user;
    }

    public function getItem() {
        return $this->item;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setItem($item) {
        $this->item = $item;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
    }
    
    /**
     * 
     * @param type $user
     * @param type $item
     * @param type $price
     * @param type $amount
     */
    public function newTransaction($user, $item, $price, $amount){
        $this->user = $user;
        $this->item = $item;
        $this->price = $price;
        $this->amount = $amount;
    }
}

